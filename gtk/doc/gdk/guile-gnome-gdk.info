This is guile-gnome-gdk.info, produced by makeinfo version 6.3 from
guile-gnome-gdk.texi.

This manual is for '(gnome gdk)' (version 2.16.5, updated 24 January
2015)

   GDK documentation Copyright 1997-2007 Damon Chaplin and others

     This work may be reproduced and distributed in whole or in part, in
     any medium, physical or electronic, so as long as this copyright
     notice remains intact and unchanged on all copies.  Commercial
     redistribution is permitted and encouraged, but you may not
     redistribute, in whole or in part, under terms more restrictive
     than those under which you received it.  If you redistribute a
     modified or translated version of this work, you must also make the
     source code to the modified or translated version available in
     electronic form without charge.  However, mere aggregation as part
     of a larger work shall not count as a modification for this
     purpose.

     All code examples in this work are placed into the public domain,
     and may be used, modified and redistributed without restriction.

     BECAUSE THIS WORK IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
     FOR THE WORK, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT
     WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
     PARTIES PROVIDE THE WORK "AS IS" WITHOUT WARRANTY OF ANY KIND,
     EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
     IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
     PURPOSE. SHOULD THE WORK PROVE DEFECTIVE, YOU ASSUME THE COST OF
     ALL NECESSARY REPAIR OR CORRECTION.

     IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN
     WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY
     MODIFY AND/OR REDISTRIBUTE THE WORK AS PERMITTED ABOVE, BE LIABLE
     TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR
     CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
     THE WORK, EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF
     THE POSSIBILITY OF SUCH DAMAGES.

   GDK-Pixbuf documentation Copyright 2000 Free Software Foundation.

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.1 or any later version published by the Free Software
     Foundation with no Invariant Sections, no Front-Cover Texts, and no
     Back-Cover Texts.  You may obtain a copy of the GNU Free
     Documentation License from the Free Software Foundation by visiting
     their Web site or by writing to:

     The Free Software Foundation, Inc., 59 Temple Place - Suite 330,
     Boston, MA 02111-1307, USA

     Many of the names used by companies to distinguish their products
     and services are claimed as trademarks.  Where those names appear
     in any GNOME documentation, and those trademarks are made aware to
     the members of the GNOME Documentation Project, the names have been
     printed in caps or initial caps.

INFO-DIR-SECTION The Algorithmic Language Scheme
START-INFO-DIR-ENTRY
* Guile-Gdk: (guile-gnome-gdk.info).  The GIMP Drawing Kit.
END-INFO-DIR-ENTRY


Indirect:
guile-gnome-gdk.info-1: 3172
guile-gnome-gdk.info-2: 307003

Tag Table:
(Indirect)
Node: Top3172
Node: Overview8405
Node: General8696
Node: GdkDisplay20701
Node: GdkDisplayManager32995
Node: GdkScreen34778
Node: Points Rectangles and Regions52054
Node: Graphics Contexts57916
Node: Drawing Primitives68048
Node: Bitmaps and Pixmaps90574
Node: GdkRGB93234
Node: Images100821
Node: Pixbufs105150
Node: Colormaps and Colors115460
Node: Visuals119967
Node: Fonts125135
Node: Cursors137770
Node: Windows145277
Node: Events203448
Node: Event Structures208831
Node: Key Values211153
Node: Selections219366
Node: Drag and Drop225362
Node: Properties and Atoms229947
Node: Threads232663
Node: Input Devices240019
Node: Pango Interaction246693
Node: Cairo Interaction257230
Node: X Window System Interaction260155
Node: The GdkPixbuf Structure263634
Node: File Loading269052
Node: Image Data in Memory273502
Node: Scaling276688
Node: Utilities287718
Node: Animations291393
Node: GdkPixbufLoader294748
Node: Module Interface307003
Node: Undocumented309629
Node: Type Index310907
Node: Function Index314447

End Tag Table
