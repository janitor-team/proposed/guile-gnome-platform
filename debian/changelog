guile-gnome-platform (2.16.5-2) unstable; urgency=medium

  * Fixed hardening.

 -- Tommi Höynälänmaa <tommi.hoynalanmaa@iki.fi>  Mon, 17 Dec 2018 11:04:03 +0200

guile-gnome-platform (2.16.5-1) unstable; urgency=medium

  * Upgraded to upstream version 2.16.5.
  * Built for Guile 2.2.
  * Added fields Vcs-Git and Vcs-Browser into debian/control.

 -- Tommi Höynälänmaa <tommi.hoynalanmaa@iki.fi>  Sun, 16 Dec 2018 10:04:14 +0200

guile-gnome-platform (2.16.4-6) unstable; urgency=medium

  * New maintainer (Closes: #743714)

 -- Tommi Höynälänmaa <tommi.hoynalanmaa@iki.fi>  Tue, 20 Nov 2018 16:04:01 +0200

guile-gnome-platform (2.16.4-5) unstable; urgency=medium

  * QA upload.
  * Drop unused guile-gnome2-canvas and guile-gnome2-gconf packages
    since they depend on unmaintained libraries

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 29 Dec 2017 15:01:11 -0500

guile-gnome-platform (2.16.4-4) unstable; urgency=low

  * QA upload.
  * Add libgnomecanvas2-dev to the build dependencies,
    it was previously pulled in by libgnomeui-dev.
  * Add signature to watch file.

 -- Adrian Bunk <bunk@debian.org>  Sun, 16 Jul 2017 14:31:17 +0300

guile-gnome-platform (2.16.4-3) unstable; urgency=low

  * QA upload.
  * Don't run tests for non-built packages, thanks to David Pirotte
    for reporting and suggesting a fix.

 -- Adrian Bunk <bunk@debian.org>  Sun, 16 Jul 2017 13:20:16 +0300

guile-gnome-platform (2.16.4-2) unstable; urgency=low

  * QA upload.
  * Remove the guile-gnome2-gnome, guile-gnome2-gnome-ui and
    guile-gnome2-vfs packages, the libraries will be removed
    in buster. (Closes: #868423)

 -- Adrian Bunk <bunk@debian.org>  Sat, 15 Jul 2017 22:00:39 +0300

guile-gnome-platform (2.16.4-1) unstable; urgency=medium

  * QA upload.
  * Imported Upstream version 2.16.4. Closes: #830406
    - Remove no longer needed patches:
      - 0001-Bug-592486-Increase-stack-size-for-GLib-tests
      - 0004-Allow-configuring-guile-and-guile-snarf-executables
      - automake-parallel-tests (results in FTBFS now)
    - Remove patches applied upstream:
      - 0006-Fix-deprecated-includes-of-glib-sub-headers
      - build-absolute-paths, applied upstream.
    - Refresh patch automake-parallel-tests

 -- Dr. Tobias Quathamer <toddy@debian.org>  Sun, 01 Jan 2017 19:31:28 +0100

guile-gnome-platform (2.16.2-2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * QA upload.
  * Set maintainer to Debian QA Group.  (See: #743714)
  * Fix lintian warning build-depends-on-1-revision.

  [ أحمد المحمودي (Ahmed El-Mahmoudy) ]
  * Sync changes from Ubuntu. (Closes: #761211)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 18 Jul 2015 11:12:10 +0200

guile-gnome-platform (2.16.2-1.1ubuntu1) trusty; urgency=medium

  * Switch to guile-2.0-dev.
  * Backport build fix from upstream to use absolute paths and handle
    GUILE_LOAD_PATH being unset.
  * Handle the Automake parallel test harness.
  * Use dh-autoreconf to help apply these changes.

 -- Colin Watson <cjwatson@ubuntu.com>  Tue, 25 Mar 2014 11:51:05 +0000

guile-gnome-platform (2.16.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules
    - use /usr/share/cdbs/1/class/autotools-vars.mk, instead of hardcoding
      autotools versions (Closes: #713193)

 -- Hideki Yamane <henrich@debian.org>  Tue, 11 Feb 2014 17:59:09 +0900

guile-gnome-platform (2.16.2-1) unstable; urgency=low

  * New upstream version:
    - obsoletes patch fixes/gtk-2.18-build.
    - other patches rebased.
    - new patch to allow building against guile-1.8.
    - new patch to preserve meaning of --disable-Werror
      configure option.
    - needs g-wrap (>= 1.9.14); Build-Depends adjusted.
  * New patch to fix deprecated glib includes (closes: #665552).
  * debian/rules:
    - Use -a flag for xvfb-run to guard against potential
      build failures.
    - Bump automake and autoconf versions (1.11 and 2.69,
      respectively) to match new upstream. Build-Depends
      adjusted.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Drop tg2quilt in favor of gbp-pq, as this requires no special
    knowledge from collaborators.

 -- Andreas Rottmann <rotty@debian.org>  Sat, 02 Jun 2012 16:30:43 +0200

guile-gnome-platform (2.16.1-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't pass -GTK_DISABLE_DEPRECATED when using libgnomeui, as
    GtkCallbackMarshal is disabled with GTK 2.24. Closes: #634292.
  * Explicitly link with orbit, fixing the test suite.
  * Finish emptying dependency_libs from the .la files. Closes: #642695.

 -- Regis Boudin <regis@debian.org>  Tue, 04 Oct 2011 19:26:06 +0100

guile-gnome-platform (2.16.1-6) unstable; urgency=low

  * Standards-Version 3.9.2, no changes.
  * Fix debian/rules wrt. cdbs deprecation warnings.
  * Tighten Build-Depends on g-wrap to ensure Bug#625187 is not
    encountered.
  * guile-gnome2-glib: Don't install public .la files (closes: #621172).

 -- Andreas Rottmann <rotty@debian.org>  Tue, 03 May 2011 01:00:31 +0200

guile-gnome-platform (2.16.1-5) unstable; urgency=low

  * New patch: fixes/snarf-newer-cpp (closes: #620264).
  * Switch package format to source format 3.0 (quilt):
    - Drop Build-Depends on quilt.
    - Removed quilt-related stuff from debian/rules.
  * Standards-Version 3.9.1:
    - Include build rules to empty dependency_libs in .la files.

 -- Andreas Rottmann <rotty@debian.org>  Fri, 01 Apr 2011 13:14:11 +0200

guile-gnome-platform (2.16.1-4) unstable; urgency=low

  * New patch: fixes/gtk-2.18-build (closes: #549752).
  * Added README.source, referring to tg2quilt HOWTO.

 -- Andreas Rottmann <rotty@debian.org>  Thu, 05 Nov 2009 16:28:43 +0100

guile-gnome-platform (2.16.1-3) unstable; urgency=low

  * Standards-Version 3.8.3:
    - Changed Section of source package to `lisp'.
  * Now using topgit with dpkg-buildpackage to manage
    the package:
    - debian/rules: Add optional tg2quilt.mk include.
  * New patch: fixes/link-with-guile-cairo (closes: #522531).

 -- Andreas Rottmann <rotty@debian.org>  Mon, 17 Aug 2009 18:53:44 +0200

guile-gnome-platform (2.16.1-2) unstable; urgency=low

  * debian/rules: Use patchsys-quilt.mk, Build-Depends
    adjusted accordingly.
  * New patch: glib-test-stack-depth.patch.
    - Increases the maximum stack depth for the glib test
      suite (closes: #490312).
  * debian/rules: Enabled DEB_AUTO_UPDATE_{AUTOMAKE,ACLOCAL,AUTOCONF,LIBTOOL},
    as the added patch touches a Makefile.am.
  * Add automake1.10, libtool to Build-Depends for the same reason.
  * Standards-Version 3.8.1 (no changes).

 -- Andreas Rottmann <rotty@debian.org>  Sat, 06 Jun 2009 16:16:33 +0200

guile-gnome-platform (2.16.1-1) unstable; urgency=low

  * New upstream release.
  * Upstream bumped API, hence all package names changed from
    guile-gnome0-* to guile-gnome2-*.
  * Standards-Version 3.8.0 (no changes).
  * Remove unused guile-gnome2-cairo.install file.
  * Added ${misc:Depends} to all packages.
  * Do not build-depend on a -1 revision of the g-wrap packages.
  * Add a proper "Copyright YEARS AUTHORS" line to debian/copyright.

 -- Andreas Rottmann <rotty@debian.org>  Sun, 01 Mar 2009 22:51:48 +0100

guile-gnome-platform (2.15.95-2) unstable; urgency=low

  * Fix build-depends (added xauth and xfonts-base, both needed by
    xvfb-run).

 -- Andreas Rottmann <rotty@debian.org>  Wed, 05 Dec 2007 13:42:44 +0100

guile-gnome-platform (2.15.95-1) unstable; urgency=low

  * New upstream release (closes: #437293).
  * Run configure with --disable-Werror (closes: #366627).
  * Updated Build-Depends to include libgwrap-runtime-dev (closes: #436318).
  * Standards-Version 3.7.3 (no changes).
  * Don't run autotools during build.
  * Remove 01_dev-upstream-20050917.patch - no longer needed.
  * Build-Depend on xvfb and use that to run the testsuite.
  * Include .so symlink in guile-gnome0-glib (closes: #355369).
  * Include manpage for guile-gnome-0.
  * Add Homepage field in debian/control.
  * Added watch file.

 -- Andreas Rottmann <rotty@debian.org>  Tue, 04 Dec 2007 22:02:10 +0100

guile-gnome-platform (2.7.99-4) unstable; urgency=high

  * Revert auto-update of control, since it's forbidden by release policy,
    see http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=311724.
  * Also update libtool (closes: #328946).

 -- Andreas Rottmann <rotty@debian.org>  Sun, 18 Sep 2005 13:36:17 +0200

guile-gnome-platform (2.7.99-3) unstable; urgency=high

  * Rebuild against current g-wrap (to further the libffi transition,
    urgency set to high).
  * Pull in upstream changes as 01_dev-upstream-20050917.patch, replacing
    the old patch. This now contains a fix for the gtk-list-XXX crashes
    and the 64bit warnings (closes: #304936).
  * Standards-Version 3.6.2 (no changes).

 -- Andreas Rottmann <rotty@debian.org>  Sat, 17 Sep 2005 14:52:02 +0200

guile-gnome-platform (2.7.99-2) unstable; urgency=low

  * Pull in upstream changes as 01_dev-upstream-20050410.patch,
    containing build fixes (closes: #303624).
    - Use simple-patchsys from CDBS.
    - Build-Depend on g-wrap >= 1.9.5-2 since upstream changes depend on
      that.
  * Auto-generate control from control.in (currently disabled due to
    #301692).

 -- Andreas Rottmann <rotty@debian.org>  Sun, 10 Apr 2005 22:25:17 +0200

guile-gnome-platform (2.7.99-1) unstable; urgency=low

  * Initial packaging (closes: #205465).

 -- Andreas Rottmann <rotty@debian.org>  Fri, 11 Mar 2005 12:09:26 +0100
