This is guile-gnome-libglade.info, produced by makeinfo version 6.3 from
guile-gnome-libglade.texi.

This manual is for '(gnome libglade)' (version 2.16.5, updated 24
January 2015)

   Copyright 1999-2002 James Henstridge

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.1 or any later version published by the Free Software
     Foundation with no Invariant Sections, no Front-Cover Texts, and no
     Back-Cover Texts.  You may obtain a copy of the GNU Free
     Documentation License from the Free Software Foundation by visiting
     their Web site or by writing to: Free Software Foundation, Inc., 59
     Temple Place - Suite 330, Boston, MA 02111-1307, USA.

INFO-DIR-SECTION The Algorithmic Language Scheme
START-INFO-DIR-ENTRY
* Guile-Libglade: (guile-gnome-libglade.info).  Load GTK+ interfaces from XML.
END-INFO-DIR-ENTRY


File: guile-gnome-libglade.info,  Node: Top,  Next: Overview,  Up: (dir)

Guile-Libglade
**************

This manual is for '(gnome libglade)' (version 2.16.5, updated 24
January 2015)

   Copyright 1999-2002 James Henstridge

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU Free Documentation License,
     Version 1.1 or any later version published by the Free Software
     Foundation with no Invariant Sections, no Front-Cover Texts, and no
     Back-Cover Texts.  You may obtain a copy of the GNU Free
     Documentation License from the Free Software Foundation by visiting
     their Web site or by writing to: Free Software Foundation, Inc., 59
     Temple Place - Suite 330, Boston, MA 02111-1307, USA.

* Menu:

* Overview::             About libglade and its Guile bindings.

* GladeXML::             Allows dynamic loading of user interfaces
                         from XML descriptions.

* Undocumented::         Undocumented functions.

* Type Index::
* Function Index::


File: guile-gnome-libglade.info,  Node: Overview,  Next: GladeXML,  Prev: Top,  Up: Top

1 Overview
**********

'(gnome glade)' wraps the libglade interface XML interface definition
library for Guile.  It is a part of Guile-GNOME.

   Glade files are XML descriptions of a GTK+ widget hierarchy.  They
are usually created with the Glade user interface editor.  A simple
example might look like this:

     (define xml-buf "<?xml version=\"1.0\" standalone=\"no\"?>
     <!DOCTYPE glade-interface SYSTEM \"http://glade.gnome.org/glade-2.0.dtd\">

     <glade-interface>

     <widget class=\"GtkWindow\" id=\"window1\">
       <property name=\"title\" translatable=\"yes\">window1</property>
       <child>
         <widget class=\"GtkLabel\" id=\"label1\">
           <property name=\"label\" translatable=\"yes\">Hello world!</property>
         </widget>
       </child>
     </widget>

     </glade-interface>")

     (define glade-xml (glade-xml-new-from-buffer xml-buf))
     (define main-window (get-widget glade-xml "window1"))
     (show-all main-window)
     (g-main-loop-run (g-main-loop-new))

   Of course, in practice you are more likely to load the XML from a
file; in that case you would use 'glade-xml-new'.

   Also interesting are the 'signal-connect' and 'signal-autoconnect'
implementations; read on for more details.

   See the documentation for '(gnome gobject)' for more information on
Guile-GNOME.


File: guile-gnome-libglade.info,  Node: GladeXML,  Next: Undocumented,  Prev: Overview,  Up: Top

2 GladeXML
**********

Allows dynamic loading of user interfaces from XML descriptions.

2.1 Overview
============

This object represents an 'instantiation' of an XML interface
description.  When one of these objects is created, the XML file is
read, and the interface is created.  The GladeXML object then provides
an interface for accessing the widgets in the interface by the names
assigned to them inside the XML description.

   The GladeXML object can also be used to connect handlers to the named
signals in the description.  Libglade also provides an interface by
which it can look up the signal handler names in the program's symbol
table and automatically connect as many handlers up as it can that way.

2.2 Usage
=========

 -- Class: <glade-xml>
     Derives from '<gobject>'.

     This class defines no direct slots.

 -- Function: glade-xml-new (fname 'mchars') (root 'mchars')
          (domain 'mchars') =>  (ret '<glade-xml>')
     Creates a new GladeXML object (and the corresponding widgets) from
     the XML file FNAME.  Optionally it will only build the interface
     from the widget node ROOT (if it is not ''#f'').  This feature is
     useful if you only want to build say a toolbar or menu from the XML
     file, but not the window it is embedded in.  Note also that the XML
     parse tree is cached to speed up creating another GladeXML object
     for the same file

     FNAME
          the XML file name.

     ROOT
          the widget node in FNAME to start building from (or ''#f'')

     DOMAIN
          the translation domain for the XML file (or ''#f'' for
          default)

     RET
          the newly created GladeXML object, or NULL on failure.

 -- Function: glade-xml-new-from-buffer (buffer 'mchars')
          (root 'mchars') (domain 'mchars') =>  (ret '<glade-xml>')
     Creates a new GladeXML object (and the corresponding widgets) from
     the buffer BUFFER.  Optionally it will only build the interface
     from the widget node ROOT (if it is not ''#f'').  This feature is
     useful if you only want to build say a toolbar or menu from the XML
     document, but not the window it is embedded in.

     BUFFER
          the memory buffer containing the XML document.

     SIZE
          the size of the buffer.

     ROOT
          the widget node in BUFFER to start building from (or ''#f'')

     DOMAIN
          the translation domain to use for this interface (or ''#f'')

     RET
          the newly created GladeXML object, or NULL on failure.

 -- Function: glade-xml-signal-connect (self '<glade-xml>')
          (handlername 'mchars') (proc 'scm')
 -- Method: signal-connect
     In the glade interface descriptions, signal handlers are specified
     for widgets by name.  This function allows you to connect a Scheme
     function to all signals in the GladeXML file with the given signal
     handler name.

     SELF
          the GladeXML object

     HANDLERNAME
          the signal handler name

     FUNC
          the signal handler function

 -- Function: glade-xml-signal-autoconnect (self '<glade-xml>')
          (module 'scm')
 -- Method: signal-autoconnect
     This function is a variation of 'glade-xml-signal-autoconnect'.  It
     will bind signal handers to the result of evaluating each signal
     handler name within MODULE.  So for example, if you have ten
     widgets defining a handler as the string '"on-clicked"', you might
     do this:

          (define (on-clicked . args)
            (display "Doing me thing...\n"))
          (signal-autoconnect glade-xml (current-module))
          ;; now the on-clicked handlers are connected

     SELF
          the GladeXML object.

     MODULE
          A scheme module in which to evaluate the signal handlers
          definitions.

 -- Function: glade-xml-get-widget (self '<glade-xml>') (name 'mchars')
          =>  (ret '<gtk-widget>')
 -- Method: get-widget
     This function is used to get a pointer to the GtkWidget
     corresponding to NAME in the interface description.  You would use
     this if you have to do anything to the widget after loading.

     SELF
          the GladeXML object.

     NAME
          the name of the widget.

     RET
          the widget matching NAME, or ''#f'' if none exists.

 -- Function: glade-xml-get-widget-prefix (self '<glade-xml>')
          (name 'mchars') =>  (ret 'glist-of')
 -- Method: get-widget-prefix
     This function is used to get a list of pointers to the GtkWidget(s)
     with names that start with the string NAME in the interface
     description.  You would use this if you have to do something to all
     of these widgets after loading.

     SELF
          the GladeXML object.

     NAME
          the name of the widget.

     RET
          A list of the widget that match NAME as the start of their
          name, or ''#f'' if none exists.

 -- Function: glade-get-widget-name (widget '<gtk-widget>') => 
          (ret 'mchars')
     Used to get the name of a widget that was generated by a GladeXML
     object.

     WIDGET
          the widget

     RET
          the name of the widget.

 -- Function: glade-get-widget-tree (widget '<gtk-widget>') => 
          (ret '<glade-xml>')
     This function is used to get the GladeXML object that built this
     widget.

     WIDGET
          the widget

     RET
          the GladeXML object that built this widget.


File: guile-gnome-libglade.info,  Node: Undocumented,  Next: Type Index,  Prev: GladeXML,  Up: Top

3 Undocumented
**************

The following symbols, if any, have not been properly documented.

3.1 (gnome gw libglade)
=======================

 -- Variable: glade-xml-relative-file


File: guile-gnome-libglade.info,  Node: Type Index,  Next: Function Index,  Prev: Undocumented,  Up: Top

Type Index
**********

 [index ]
* Menu:

* <glade-xml>:                           GladeXML.             (line 25)


File: guile-gnome-libglade.info,  Node: Function Index,  Prev: Type Index,  Up: Top

Function Index
**************

 [index ]
* Menu:

* get-widget:                            GladeXML.            (line 118)
* get-widget-prefix:                     GladeXML.            (line 134)
* glade-get-widget-name:                 GladeXML.            (line 150)
* glade-get-widget-tree:                 GladeXML.            (line 161)
* glade-xml-get-widget:                  GladeXML.            (line 115)
* glade-xml-get-widget-prefix:           GladeXML.            (line 132)
* glade-xml-new:                         GladeXML.            (line  30)
* glade-xml-new-from-buffer:             GladeXML.            (line  53)
* glade-xml-signal-autoconnect:          GladeXML.            (line  94)
* glade-xml-signal-connect:              GladeXML.            (line  76)
* signal-autoconnect:                    GladeXML.            (line  96)
* signal-connect:                        GladeXML.            (line  79)



Tag Table:
Node: Top937
Node: Overview1986
Node: GladeXML3412
Node: Undocumented8911
Node: Type Index9199
Node: Function Index9425

End Tag Table
