This is guile-gnome-libgnomeui.info, produced by makeinfo version 6.3
from guile-gnome-libgnomeui.texi.

This manual is for '(gnome libgnomeui)' (version 2.16.5, updated 24
January 2015)

   Copyright 1998-2007 Kjartan Maraas, Anders Carlsson, and others

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU General Public License, Version
     2 or any later version published by the Free Software Foundation.

INFO-DIR-SECTION The Algorithmic Language Scheme
START-INFO-DIR-ENTRY
* Guile-Libgnomeui: (guile-gnome-libgnomeui.info).  A deprecated GNOME UI lib.
END-INFO-DIR-ENTRY


File: guile-gnome-libgnomeui.info,  Node: Top,  Next: Overview,  Up: (dir)

Guile-Libgnomeui
****************

This manual is for '(gnome libgnomeui)' (version 2.16.5, updated 24
January 2015)

   Copyright 1998-2007 Kjartan Maraas, Anders Carlsson, and others

     Permission is granted to copy, distribute and/or modify this
     document under the terms of the GNU General Public License, Version
     2 or any later version published by the Free Software Foundation.

* Menu:

* Overview::             About libgnomeui and its Guile bindings.

* GnomeAuthentication:: 
* GnomeClient::          Interactions with the session manager.
* GnomeDateEdit:: 
* GnomeMultiScreen:: 
* GnomePassword:: 
* gnome-vfs-util:: 

* Undocumented::         Undocumented functions.

* Type Index::
* Function Index::


File: guile-gnome-libgnomeui.info,  Node: Overview,  Next: GnomeAuthentication,  Prev: Top,  Up: Top

1 Overview
**********

'(gnome gnome-ui)' wraps some pieces of the libgnomeui library for
Guile.  It is a part of Guile-GNOME.

   Libgnomeui historically was a staging ground for code meant to go
into GTK. In the modern environment, it has largely been replaced, its
functionality having been pushed into GTK+ itself.

   However, as of 2008 it still has some utility as a kind of glue layer
between lower-level modules, with the authentication manager, the
GNOME-VFS GDK-Pixbuf integration, and the session manager code.

   See the documentation for '(gnome gobject)' for more information on
Guile-GNOME.


File: guile-gnome-libgnomeui.info,  Node: GnomeAuthentication,  Next: GnomeClient,  Prev: Overview,  Up: Top

2 GnomeAuthentication
*********************

2.1 Overview
============

2.2 Usage
=========

 -- Function: gnome-authentication-manager-init
     This function checks for thread support and does a thread
     initialisation, if the support is available.  Also sets the default
     sync and async gnome-vfs callbacks for various types of
     authentication.

     Note: If you call this, and you use threads with gtk+, you must
     never hold the gdk lock while doing synchronous gnome-vfs calls.
     Otherwise an authentication callback presenting a dialog could try
     to grab the already held gdk lock, causing a deadlock.

     Since 2.4


File: guile-gnome-libgnomeui.info,  Node: GnomeClient,  Next: GnomeDateEdit,  Prev: GnomeAuthentication,  Up: Top

3 GnomeClient
*************

Interactions with the session manager.

3.1 Overview
============

3.2 Usage
=========

 -- Class: <gnome-client>
     Derives from '<gtk-object>'.

     This class defines no direct slots.

 -- Signal on <gnome-client>: save-yourself (arg0 '<gint>')
          (arg1 '<gnome-save-style>') (arg2 '<gboolean>')
          (arg3 '<gnome-interact-style>') (arg4 '<gboolean>')
          => '<gboolean>'
     Called when either a "SaveYourself" or a "SaveYourselfPhase2" call
     is made by the session manager.

 -- Signal on <gnome-client>: die
     Called when the session manager wants the client to shut down.

 -- Signal on <gnome-client>: save-complete
     Called when the session manager has finished checkpointing all of
     the clients.  Clients are then free to change their state.

 -- Signal on <gnome-client>: shutdown-cancelled
     Called if the session manager had sent a "SaveYourself" to all
     clients in preparation for shutting down and the shutdown was then
     cancelled.  A client can then continue running and change its
     state.

 -- Signal on <gnome-client>: connect (arg0 '<gboolean>')
     Called once the client has been connected to the signal manager.

 -- Signal on <gnome-client>: disconnect
     Called when the client is disconnected from the session manager.

 -- Function: gnome-master-client =>  (ret '<gnome-client>')
     Get the master session management client.  This master client gets
     a client id, that may be specified by the '-sm-client-id' command
     line option.  A master client will be generated by
     'gnome-program-init'.  If possible the master client will contact
     the session manager after command-line parsing is finished (unless
     'gnome-client-disable-master-connection' was called).  The master
     client will also set the SM_CLIENT_ID property on the client leader
     window of your application.

     Additionally, the master client gets some static arguments set
     automatically (see 'gnome-client-add-static-arg' for static
     arguments): 'gnome-program-init' passes all the command line
     options which are recognised by gtk as static arguments to the
     master client.

     RET
          Pointer to the master client

 -- Function: gnome-client-get-config-prefix (self '<gnome-client>') => 
          (ret 'mchars')
 -- Method: get-config-prefix
     Get the config prefix for a client.  This config prefix provides a
     suitable place to store any details about the state of the client
     which can not be described using the app's command line arguments
     (as set in the restart command).  You may push the returned value
     using 'gnome-config-push-prefix' and read or write any values you
     require.

     CLIENT
          Pointer to GNOME session client object.

     RET
          Config prefix.  The returned string belongs to libgnomeui
          library and should NOT be freed by the caller.

 -- Function: gnome-client-get-flags (self '<gnome-client>') => 
          (ret '<gnome-client-flags>')
 -- Method: get-flags
     Determine the client's status with the session manager.,

     CLIENT
          Pointer to GNOME session client object.

     RET
          Various '<gnome-client-flag>' flags which have been or'd
          together.

 -- Function: gnome-client-set-restart-style (self '<gnome-client>')
          (style '<gnome-restart-style>')
 -- Method: set-restart-style
     Tells the session manager how the client should be restarted in
     future session.  The options are given by the
     '<gnome-restart-style>' enum.

     CLIENT
          Pointer to GNOME session client object.

     STYLE
          When to restart the client.

 -- Function: gnome-client-set-priority (self '<gnome-client>')
          (priority 'unsigned-int')
 -- Method: set-priority
     The gnome-session manager restarts clients in order of their
     priorities in a similar way to the start up ordering in SysV. This
     function allows the app to suggest a position in this ordering.
     The value should be between 0 and 99.  A default value of 50 is
     assigned to apps that do not provide a value.  The user may assign
     a different priority.

     CLIENT
          Pointer to GNOME session client object.

     PRIORITY
          Position of client in session start up ordering.

 -- Function: gnome-client-set-current-directory (self '<gnome-client>')
          (dir 'mchars')
 -- Method: set-current-directory
     Set the directory to be in when running shutdown, discard, restart,
     etc.  commands.

     CLIENT
          Pointer to GNOME session client object.

     DIR
          Directory path.

 -- Function: gnome-client-set-environment (self '<gnome-client>')
          (name 'mchars') (value 'mchars')
 -- Method: set-environment
     Set an environment variable to be placed in the client's
     environment prior to running restart, shutdown, discard, etc.
     commands.

     CLIENT
          Pointer to GNOME session client object.

     NAME
          Name of the environment variable

     VALUE
          Value of the environment variable

 -- Function: gnome-client-set-process-id (self '<gnome-client>')
          (pid 'int')
 -- Method: set-process-id
     The client should tell the session manager the result of 'getpid'.
     However, GNOME does this automatically; so you do not need this
     function.

     CLIENT
          Pointer to GNOME session client object.

     PID
          PID to set as the client's PID.

 -- Function: gnome-client-set-program (self '<gnome-client>')
          (program 'mchars')
 -- Method: set-program
     Used to tell the session manager the name of your program.  Set
     automatically; this function isn't needed.

     CLIENT
          Pointer to GNOME session client object.

     PROGRAM
          Name of the program.

 -- Function: gnome-client-set-user-id (self '<gnome-client>')
          (id 'mchars')
 -- Method: set-user-id
     Tell the session manager the user's login name.  GNOME does this
     automatically; no need to call the function.

     CLIENT
          Pointer to GNOME session client object.

     ID
          Username.

 -- Function: gnome-client-save-any-dialog (self '<gnome-client>')
          (dialog '<gtk-dialog>')
 -- Method: save-any-dialog
     May be called during a "save_youself" handler to request that a
     (modal) dialog is presented to the user.  The session manager
     decides when the dialog is shown, but it will not be shown it
     unless the session manager is sending an interaction style of
     '<gnome-interact-any>'.  A "Cancel Logout" button will be added
     during a shutdown.

     CLIENT
          Pointer to '<gnome-client>' object.

     DIALOG
          Pointer to GNOME dialog widget (a '<gtk-dialog>' widget).

 -- Function: gnome-client-save-error-dialog (self '<gnome-client>')
          (dialog '<gtk-dialog>')
 -- Method: save-error-dialog
     May be called during a "save_youself" handler when an error has
     occurred during the save.  The session manager decides when the
     dialog is shown, but it will not be shown it unless the session
     manager is sending an interaction style of '<gnome-interact-any>'.
     A "Cancel Logout" button will be added during a shutdown.

     CLIENT
          Pointer to '<gnome-client>' object.

     DIALOG
          Pointer to GNOME dialog widget (a '<gtk-dialog>' widget).

 -- Function: gnome-client-request-phase-2 (self '<gnome-client>')
 -- Method: request-phase-2
     Request the session managaer to emit the "save_yourself" signal for
     a second time after all the clients in the session have ceased
     interacting with the user and entered an idle state.  This might be
     useful if your app manages other apps and requires that they are in
     an idle state before saving its final data.

     CLIENT
          A '<gnome-client>' object.

 -- Function: gnome-client-request-save (self '<gnome-client>')
          (save_style '<gnome-save-style>') (shutdown 'bool')
          (interact_style '<gnome-interact-style>') (fast 'bool')
          (global 'bool')
 -- Method: request-save
     Request the session manager to save the session in some way.  The
     arguments correspond with the arguments passed to the
     "save_yourself" signal handler.

     The save_style indicates whether the save should affect data
     accessible to other users ('<gnome-save-global>') or only the state
     visible to the current user ('<gnome-save-local>') or both.
     Setting shutdown to ''#t'' will initiate a logout.  The
     interact_style specifies which kinds of interaction will be
     available.  Setting fast to ''#t'' will limit the save to setting
     the session manager properties plus any essential data.  Setting
     the value of global to ''#t'' will request that all the other apps
     in the session do a save as well.  A global save is mandatory when
     doing a shutdown.

     CLIENT
          Pointer to GNOME session client object.

     SAVE-STYLE
          Save style to request.

     SHUTDOWN
          Whether to log out of the session.

     INTERACT-STYLE
          Whether to allow user interaction.

     FAST
          Minimize activity to save as soon as possible.

     GLOBAL
          Request that all other apps in the session also save their
          state.

 -- Function: gnome-client-flush (self '<gnome-client>')
 -- Method: flush
     This will force the underlying connection to the session manager to
     be flushed.  This is useful if you have some pending changes that
     you want to make sure get committed.

     CLIENT
          A '<gnome-client>' instance.

 -- Function: gnome-client-new =>  (ret '<gnome-client>')
     Allocates memory for a new GNOME session management client object.
     After allocating, the client tries to connect to a session manager.
     You probably want to use 'gnome-master-client' instead.

     RET
          Pointer to a newly allocated GNOME session management client
          object.

 -- Function: gnome-client-new-without-connection => 
          (ret '<gnome-client>')
     Allocates memory for a new GNOME session management client object.
     You probably want to use 'gnome-master-client' instead.

     RET
          Pointer to a newly allocated GNOME session management client
          object.

 -- Function: gnome-client-connect (self '<gnome-client>')
 -- Method: connect
     Causes the client to connect to the session manager.  Usually
     happens automatically; no need to call this function.

     CLIENT
          A '<gnome-client>' instance.

 -- Function: gnome-client-disconnect (self '<gnome-client>')
 -- Method: disconnect
     Disconnect the client from the session manager.

     CLIENT
          A '<gnome-client>' instance.

 -- Function: gnome-client-set-id (self '<gnome-client>') (id 'mchars')
 -- Method: set-id
     Set the client's session management ID; must be done before
     connecting to the session manager.  There is usually no reason to
     call this function.

     CLIENT
          A '<gnome-client>' instance.

     ID
          Session management ID.

 -- Function: gnome-client-get-id (self '<gnome-client>') => 
          (ret 'mchars')
 -- Method: get-id
     Returns session management ID

     CLIENT
          A '<gnome-client>' instance.

     RET
          Session management ID for this client; ''#f'' if not connected
          to a session manager.

 -- Function: gnome-client-get-previous-id (self '<gnome-client>') => 
          (ret 'mchars')
 -- Method: get-previous-id
     Get the session management ID from the previous session.

     CLIENT
          A '<gnome-client>' instance.

     RET
          Pointer to the session management ID the client had in the
          last session, or ''#f'' if it was not in a previous session.

 -- Function: gnome-client-get-desktop-id (self '<gnome-client>') => 
          (ret 'mchars')
 -- Method: get-desktop-id
     Get the client ID of the desktop's current instance, i.e.  if you
     consider the desktop as a whole as a session managed app, this
     returns its session ID using a GNOME extension to session
     management.  May return ''#f'' for apps not running under a recent
     version of gnome-session; apps should handle that case.

     CLIENT
          A '<gnome-client>' instance.

     RET
          Session ID of GNOME desktop instance, or ''#f'' if none.


File: guile-gnome-libgnomeui.info,  Node: GnomeDateEdit,  Next: GnomeMultiScreen,  Prev: GnomeClient,  Up: Top

4 GnomeDateEdit
***************

4.1 Overview
============

4.2 Usage
=========

 -- Class: <gnome-date-edit>
     Derives from '<gtk-hbox>'.

     This class defines the following slots:

     'time'
          The time currently selected

     'dateedit-flags'
          Flags for how DateEdit looks

     'lower-hour'
          Lower hour in the time popup selector

     'upper-hour'
          Upper hour in the time popup selector

     'initial-time'
          The initial time

 -- Signal on <gnome-date-edit>: time-changed

 -- Signal on <gnome-date-edit>: date-changed

 -- Function: gnome-date-edit-new (the_time 'long') (show_time 'bool')
          (use_24_format 'bool') =>  (ret '<gtk-widget>')
     Creates a new '<gnome-date-edit>' widget which can be used to
     provide an easy to use way for entering dates and times.  If
     THE-TIME is 0 then current time is used.

     THE-TIME
          date and time to be displayed on the widget

     SHOW-TIME
          whether time should be displayed

     USE-24-FORMAT
          whether 24-hour format is desired for the time display.

     RET
          a new '<gnome-date-edit>' widget.

 -- Function: gnome-date-edit-new-flags (the_time 'long')
          (flags '<gnome-date-edit-flags>') =>  (ret '<gtk-widget>')
     Creates a new '<gnome-date-edit>' widget with the specified flags.
     If THE-TIME is 0 then current time is used.

     THE-TIME
          The initial time for the date editor.

     FLAGS
          A bitmask of GnomeDateEditFlags values.

     RET
          the newly-created date editor widget.

 -- Function: gnome-date-edit-construct (self '<gnome-date-edit>')
          (the_time 'long') (flags '<gnome-date-edit-flags>')
 -- Method: construct
     For language bindings and subclassing only

     GDE
          The '<gnome-date-edit>' object to construct

     THE-TIME
          The initial time for the date editor.

     FLAGS
          A bitmask of GnomeDateEditFlags values.

 -- Function: gnome-date-edit-set-time (self '<gnome-date-edit>')
          (the_time 'long')
 -- Method: set-time
     Changes the displayed date and time in the GnomeDateEdit widget to
     be the one represented by THE-TIME.  If THE-TIME is 0 then current
     time is used.

     GDE
          the GnomeDateEdit widget

     THE-TIME
          The time and date that should be set on the widget

 -- Function: gnome-date-edit-get-time (self '<gnome-date-edit>') => 
          (ret 'long')
 -- Method: get-time
     GDE
          The GnomeDateEdit widget

     RET
          the time entered in the GnomeDateEdit widget

 -- Function: gnome-date-edit-set-popup-range (self '<gnome-date-edit>')
          (low_hour 'int') (up_hour 'int')
 -- Method: set-popup-range
     Sets the range of times that will be provide by the time popup
     selectors.

     GDE
          The GnomeDateEdit widget

     LOW-HOUR
          low boundary for the time-range display popup.

     UP-HOUR
          upper boundary for the time-range display popup.

 -- Function: gnome-date-edit-set-flags (self '<gnome-date-edit>')
          (flags '<gnome-date-edit-flags>')
 -- Method: set-flags
     Changes the display flags on an existing date editor widget.

     GDE
          The date editor widget whose flags should be changed.

     FLAGS
          The new bitmask of GnomeDateEditFlags values.

 -- Function: gnome-date-edit-get-flags (self '<gnome-date-edit>') => 
          (ret 'int')
 -- Method: get-flags
     Queries the display flags on a date editor widget.

     GDE
          The date editor whose flags should be queried.

     RET
          The current display flags for the specified date editor
          widget.

 -- Function: gnome-date-edit-get-initial-time
          (self '<gnome-date-edit>') =>  (ret 'long')
 -- Method: get-initial-time
     Queries the initial time that was set using the
     '<gnome-date-edit-set-time>' or during creation

     GDE
          The date editor whose initial time should be queried

     RET
          The initial time in seconds (standard time_t format)


File: guile-gnome-libgnomeui.info,  Node: GnomeMultiScreen,  Next: GnomePassword,  Prev: GnomeDateEdit,  Up: Top

5 GnomeMultiScreen
******************

5.1 Overview
============

5.2 Usage
=========

 -- Function: gnome-url-show-on-screen (url 'mchars')
          (screen '<gdk-screen>') =>  (ret 'bool')
     Like 'gnome-url-show', but ensures that the viewer application
     appears on SCREEN.

     URL
          The url to display.  Should begin with the protocol to use
          (e.g.  "http:", "ghelp:", etc)

     SCREEN
          a '<gdk-screen>'.

     ERROR
          Used to store any errors that result from trying to display
          the URL.

     RET
          ''#t'' if everything went fine, ''#f'' otherwise (in which
          case ERROR will contain the actual error).

     Since 2.6

 -- Function: gnome-help-display-on-screen (file_name 'mchars')
          (link_id 'mchars') (screen '<gdk-screen>') =>  (ret 'bool')
     Like 'gnome-help-display', but ensures that the help viewer
     application appears on SCREEN.

     FILE-NAME
          The name of the help document to display.

     LINK-ID
          Can be ''#f''.  If set, refers to an anchor or section id
          within the requested document.

     SCREEN
          a '<gdk-screen>'.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).

     Since 2.6

 -- Function: gnome-help-display-uri-on-screen (help_uri 'mchars')
          (screen '<gdk-screen>') =>  (ret 'bool')
     Like 'gnome-help-display-uri', but ensures that the help viewer
     application appears on SCREEN.

     HELP-URI
          The URI to display.

     SCREEN
          a '<gdk-screen>'.

     ERROR
          A '<g-error>' instance that will hold the specifics of any
          error which occurs during processing, or ''#f''

     RET
          ''#t'' on success, ''#f'' otherwise (in which case ERROR will
          contain the actual error).

     Since 2.6


File: guile-gnome-libgnomeui.info,  Node: GnomePassword,  Next: gnome-vfs-util,  Prev: GnomeMultiScreen,  Up: Top

6 GnomePassword
***************

6.1 Overview
============

6.2 Usage
=========

 -- Class: <gnome-password-dialog>
     Derives from '<gtk-dialog>'.

     This class defines the following slots:

     'show-username'
          show-username

     'show-domain'
          show-domain

     'show-password'
          show-password

     'show-new-password'
          show-new-password

     'show-new-password-quality'
          show-new-password-quality

     'show-userpass-buttons'
          show-userpass-buttons

     'show-remember'
          show-remember

     'readonly-username'
          readonly-username

     'readonly-domain'
          readonly-domain

     'anonymous'
          anonymous

     'remember-mode'
          remember-mode

     'message'
          message

     'message-markup'
          message-markup

     'username'
          username

     'domain'
          domain

     'password'
          password

     'new-password'
          new-password

 -- Function: gnome-password-dialog-anon-selected
          (self '<gnome-password-dialog>') =>  (ret 'bool')
 -- Method: anon-selected
     Checks whether anonymous support is set to '#t' and the radio
     button for connecting as anonymous user is active.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     RET
          '#t' if anonymous support is set and the radio button is
          active, '#f' otherwise.

 -- Function: gnome-password-dialog-get-domain
          (self '<gnome-password-dialog>') =>  (ret 'mchars')
 -- Method: get-domain
     Gets the domain name from the password dialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     RET
          The domain name, a char*.

     Since 2.4

 -- Function: gnome-password-dialog-get-password
          (self '<gnome-password-dialog>') =>  (ret 'mchars')
 -- Method: get-password
     Gets the password from the password dialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     RET
          The password, a char*.

     Since 2.4

 -- Function: gnome-password-dialog-get-username
          (self '<gnome-password-dialog>') =>  (ret 'mchars')
 -- Method: get-username
     Gets the username from the password dialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     RET
          The username, a char*.

     Since 2.4

 -- Function: gnome-password-dialog-new (dialog_title 'mchars')
          (message 'mchars') (username 'mchars') (password 'mchars')
          (readonly_username 'bool') =>  (ret '<gtk-widget>')
     Creates a new password dialog with an optional title, message,
     username, password etc.  The user will be given the option to save
     the password for this session only or store it permanently in her
     keyring.

     DIALOG-TITLE
          The title of the dialog

     MESSAGE
          Message text for the dialog

     USERNAME
          The username to be used in the dialog

     PASSWORD
          Password to be used

     READONLY-USERNAME
          Boolean value that controls whether the user can edit the
          username or not

     RET
          A new password dialog.

     Since 2.4

 -- Function: gnome-password-dialog-run-and-block
          (self '<gnome-password-dialog>') =>  (ret 'bool')
 -- Method: run-and-block
     Gets the user input from PasswordDialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     RET
          ''#t'' if "Connect" button is pressed.  ''#f'' if "Cancel"
          button is pressed.

     Since 2.4

 -- Function: gnome-password-dialog-set-domain
          (self '<gnome-password-dialog>') (domain 'mchars')
 -- Method: set-domain
     Sets the domain field in the password dialog to DOMAIN.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     DOMAIN
          The domain that should be set

     Since 2.4

 -- Function: gnome-password-dialog-set-password
          (self '<gnome-password-dialog>') (password 'mchars')
 -- Method: set-password
     Sets the password in the password dialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     PASSWORD
          The password that should be set

     Since 2.4

 -- Function: gnome-password-dialog-set-remember
          (self '<gnome-password-dialog>')
          (remember '<gnome-password-dialog-remember>')
 -- Method: set-remember
     Based on the value of '<gnome-password-dialog-remember>', sets the
     state of the check buttons to remember password for the session and
     save password to keyring .

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'.

     REMEMBER
          A '<gnome-password-dialog-remember>'.

     Since 2.6

 -- Function: gnome-password-dialog-set-username
          (self '<gnome-password-dialog>') (username 'mchars')
 -- Method: set-username
     Sets the username in the password dialog.

     PASSWORD-DIALOG
          A '<gnome-password-dialog>'

     USERNAME
          The username that should be set

     Since 2.4


File: guile-gnome-libgnomeui.info,  Node: gnome-vfs-util,  Next: Undocumented,  Prev: GnomePassword,  Up: Top

7 gnome-vfs-util
****************

7.1 Overview
============

7.2 Usage
=========

 -- Function: gnome-gdk-pixbuf-new-from-uri (uri 'mchars') => 
          (ret '<gdk-pixbuf>')
     Loads a GdkPixbuf from the image file URI points to, scaling it to
     the desired size.  If you pass -1 for WIDTH or HEIGHT then the
     value specified in the file will be used.

     When preserving aspect ratio, if both height and width are set the
     size is picked such that the scaled image fits in a width * height
     rectangle.

     URI
          the uri of an image

     RET
          The loaded pixbuf, or NULL on error

     Since 2.14


File: guile-gnome-libgnomeui.info,  Node: Undocumented,  Next: Type Index,  Prev: gnome-vfs-util,  Up: Top

8 Undocumented
**************

The following symbols, if any, have not been properly documented.

8.1 (gnome gw libgnomeui)
=========================

 -- Variable: gnome-authentication-manager-dialog-is-visible

 -- Variable: gnome-client-get-global-config-prefix

 -- Variable: gnome-client-set-global-config-prefix

 -- Variable: gnome-gdk-pixbuf-new-from-uri-at-scale

 -- Function: gnome-help-display-desktop-on-screen

 -- Function: gnome-help-display-with-doc-id-on-screen

 -- Variable: gnome-password-dialog-get-remember

 -- Variable: gnome-password-dialog-set-readonly-domain

 -- Variable: gnome-password-dialog-set-readonly-username

 -- Variable: gnome-password-dialog-set-show-domain

 -- Variable: gnome-password-dialog-set-show-password

 -- Variable: gnome-password-dialog-set-show-remember

 -- Variable: gnome-password-dialog-set-show-username

 -- Variable: gnome-password-dialog-set-show-userpass-buttons


File: guile-gnome-libgnomeui.info,  Node: Type Index,  Next: Function Index,  Prev: Undocumented,  Up: Top

Type Index
**********

 [index ]
* Menu:

* <gnome-client>:                        GnomeClient.          (line 14)
* <gnome-date-edit>:                     GnomeDateEdit.        (line 12)
* <gnome-password-dialog>:               GnomePassword.        (line 12)


File: guile-gnome-libgnomeui.info,  Node: Function Index,  Prev: Type Index,  Up: Top

Function Index
**************

 [index ]
* Menu:

* anon-selected:                         GnomePassword.       (line  70)
* connect:                               GnomeClient.         (line 304)
* connect on <gnome-client>:             GnomeClient.         (line  39)
* construct:                             GnomeDateEdit.       (line  72)
* date-changed on <gnome-date-edit>:     GnomeDateEdit.       (line  34)
* die on <gnome-client>:                 GnomeClient.         (line  26)
* disconnect:                            GnomeClient.         (line 312)
* disconnect on <gnome-client>:          GnomeClient.         (line  42)
* flush:                                 GnomeClient.         (line 277)
* get-config-prefix:                     GnomeClient.         (line  66)
* get-desktop-id:                        GnomeClient.         (line 358)
* get-domain:                            GnomePassword.       (line  83)
* get-flags:                             GnomeClient.         (line  83)
* get-flags <1>:                         GnomeDateEdit.       (line 138)
* get-id:                                GnomeClient.         (line 332)
* get-initial-time:                      GnomeDateEdit.       (line 150)
* get-password:                          GnomePassword.       (line  96)
* get-previous-id:                       GnomeClient.         (line 345)
* get-time:                              GnomeDateEdit.       (line 101)
* get-username:                          GnomePassword.       (line 109)
* gnome-authentication-manager-init:     GnomeAuthentication. (line  12)
* gnome-client-connect:                  GnomeClient.         (line 303)
* gnome-client-disconnect:               GnomeClient.         (line 311)
* gnome-client-flush:                    GnomeClient.         (line 276)
* gnome-client-get-config-prefix:        GnomeClient.         (line  64)
* gnome-client-get-desktop-id:           GnomeClient.         (line 355)
* gnome-client-get-flags:                GnomeClient.         (line  81)
* gnome-client-get-id:                   GnomeClient.         (line 330)
* gnome-client-get-previous-id:          GnomeClient.         (line 342)
* gnome-client-new:                      GnomeClient.         (line 285)
* gnome-client-new-without-connection:   GnomeClient.         (line 294)
* gnome-client-request-phase-2:          GnomeClient.         (line 223)
* gnome-client-request-save:             GnomeClient.         (line 234)
* gnome-client-save-any-dialog:          GnomeClient.         (line 190)
* gnome-client-save-error-dialog:        GnomeClient.         (line 207)
* gnome-client-set-current-directory:    GnomeClient.         (line 124)
* gnome-client-set-environment:          GnomeClient.         (line 136)
* gnome-client-set-id:                   GnomeClient.         (line 318)
* gnome-client-set-priority:             GnomeClient.         (line 107)
* gnome-client-set-process-id:           GnomeClient.         (line 153)
* gnome-client-set-program:              GnomeClient.         (line 166)
* gnome-client-set-restart-style:        GnomeClient.         (line  93)
* gnome-client-set-user-id:              GnomeClient.         (line 178)
* gnome-date-edit-construct:             GnomeDateEdit.       (line  69)
* gnome-date-edit-get-flags:             GnomeDateEdit.       (line 135)
* gnome-date-edit-get-initial-time:      GnomeDateEdit.       (line 148)
* gnome-date-edit-get-time:              GnomeDateEdit.       (line  98)
* gnome-date-edit-new:                   GnomeDateEdit.       (line  36)
* gnome-date-edit-new-flags:             GnomeDateEdit.       (line  55)
* gnome-date-edit-set-flags:             GnomeDateEdit.       (line 123)
* gnome-date-edit-set-popup-range:       GnomeDateEdit.       (line 108)
* gnome-date-edit-set-time:              GnomeDateEdit.       (line  84)
* gnome-gdk-pixbuf-new-from-uri:         gnome-vfs-util.      (line  12)
* gnome-help-display-desktop-on-screen:  Undocumented.        (line  19)
* gnome-help-display-on-screen:          GnomeMultiScreen.    (line  34)
* gnome-help-display-uri-on-screen:      GnomeMultiScreen.    (line  60)
* gnome-help-display-with-doc-id-on-screen: Undocumented.     (line  21)
* gnome-master-client:                   GnomeClient.         (line  45)
* gnome-password-dialog-anon-selected:   GnomePassword.       (line  68)
* gnome-password-dialog-get-domain:      GnomePassword.       (line  81)
* gnome-password-dialog-get-password:    GnomePassword.       (line  94)
* gnome-password-dialog-get-username:    GnomePassword.       (line 107)
* gnome-password-dialog-new:             GnomePassword.       (line 120)
* gnome-password-dialog-run-and-block:   GnomePassword.       (line 151)
* gnome-password-dialog-set-domain:      GnomePassword.       (line 165)
* gnome-password-dialog-set-password:    GnomePassword.       (line 178)
* gnome-password-dialog-set-remember:    GnomePassword.       (line 191)
* gnome-password-dialog-set-username:    GnomePassword.       (line 207)
* gnome-url-show-on-screen:              GnomeMultiScreen.    (line  12)
* request-phase-2:                       GnomeClient.         (line 224)
* request-save:                          GnomeClient.         (line 241)
* run-and-block:                         GnomePassword.       (line 153)
* save-any-dialog:                       GnomeClient.         (line 193)
* save-complete on <gnome-client>:       GnomeClient.         (line  29)
* save-error-dialog:                     GnomeClient.         (line 210)
* save-yourself on <gnome-client>:       GnomeClient.         (line  19)
* set-current-directory:                 GnomeClient.         (line 126)
* set-domain:                            GnomePassword.       (line 167)
* set-environment:                       GnomeClient.         (line 139)
* set-flags:                             GnomeDateEdit.       (line 126)
* set-id:                                GnomeClient.         (line 319)
* set-password:                          GnomePassword.       (line 180)
* set-popup-range:                       GnomeDateEdit.       (line 110)
* set-priority:                          GnomeClient.         (line 110)
* set-process-id:                        GnomeClient.         (line 155)
* set-program:                           GnomeClient.         (line 168)
* set-remember:                          GnomePassword.       (line 194)
* set-restart-style:                     GnomeClient.         (line  96)
* set-time:                              GnomeDateEdit.       (line  87)
* set-user-id:                           GnomeClient.         (line 180)
* set-username:                          GnomePassword.       (line 209)
* shutdown-cancelled on <gnome-client>:  GnomeClient.         (line  33)
* time-changed on <gnome-date-edit>:     GnomeDateEdit.       (line  32)



Tag Table:
Node: Top636
Node: Overview1442
Node: GnomeAuthentication2155
Node: GnomeClient2915
Node: GnomeDateEdit15474
Node: GnomeMultiScreen19665
Node: GnomePassword21802
Node: gnome-vfs-util26875
Node: Undocumented27627
Node: Type Index28665
Node: Function Index29039

End Tag Table
